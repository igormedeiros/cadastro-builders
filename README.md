# Cadastro de Clientes - Builders

#### API

- Para teste da api, segue na raiz do projeto o Json com as configuracoes das chamadas.



#### Requisitos

- JDK 11
- Maven
- Docker
- Docker-compose

#### Start project


#####Abrir terminal
- ```mvn clean install```
- ```cd .docker/```
- ```sudo docker-compose up```


#####Abrir novo terminal
- ```cd db/```
- ```mvn flyway::clean flyway::migrate```