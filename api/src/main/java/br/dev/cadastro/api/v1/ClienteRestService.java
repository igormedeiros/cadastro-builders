package br.dev.cadastro.api.v1;

import br.dev.cadastro.api.v1.dto.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, value = "Servico referente a manutencao de clientes")
@RequestMapping(path = ClienteRestService.PATH, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface ClienteRestService {

    String PATH = "/v1";
    String CLIENTES = "/cliente";
    String PAGINADA = CLIENTES + "/paginada";
    String CLIENTE_ID = "clienteId";
    String CLIENTE_ID_PATH_PARAM = CLIENTES + "/" + "{" + CLIENTE_ID + "}";
    String DESATIVAR = CLIENTE_ID_PATH_PARAM + "/desativar";
    String ATIVAR = CLIENTE_ID_PATH_PARAM + "/ativar";

    @ApiOperation(
            value = "Cria um cliente de acordo com os parametros informados",
            response = ClienteDTO.class
    )
    @PostMapping(path = CLIENTES)
    ClienteDTO criar(
            @RequestBody
                    ClienteParametrosDTO parametros
    );

    @DeleteMapping(path = CLIENTE_ID_PATH_PARAM)
    void deletar(
            @ApiParam(value = "Id para deletar cliente", required = true)
            @PathVariable(CLIENTE_ID)
                    Integer idCliente

    );

    @ApiOperation(
            value = "Altera um cliente de acordo com os parametros informados",
            response = ClienteDTO.class
    )
    @PutMapping(path = CLIENTE_ID_PATH_PARAM)
    ClienteDTO alterar(
            @ApiParam(value = "Id do cliente para ser ativado", required = true)
            @PathVariable(CLIENTE_ID)
                    Integer idCliente,
            @RequestBody
                    ClienteParametrosAlteracaoDTO parametros
    );

    @ApiOperation(
            value = "Recupera todos os clientes cadastrados paginado",
            response = PaginaClienteDTO.class
    )
    @GetMapping(path = PAGINADA)
    PaginaClienteDTO listar(PessoaQueryDTO queryDTO);


    @ApiOperation(
            value = "Desativa um cliente",
            response = ClienteDTO.class
    )
    @PatchMapping(path = DESATIVAR)
    ClienteDTO desativar(
            @ApiParam(value = "Id do cliente para ser desativado", required = true)
            @PathVariable(CLIENTE_ID)
                    Integer idCliente

    );

    @ApiOperation(
            value = "Ativa um cliente",
            response = ClienteDTO.class
    )
    @PatchMapping(path = ATIVAR)
    ClienteDTO ativar(
            @ApiParam(value = "Id do cliente para ser ativado", required = true)
            @PathVariable(CLIENTE_ID)
                    Integer idCliente

    );

    @ApiOperation(
            value = "Pesquisar um cliente",
            response = ClienteDTO.class
    )
    @GetMapping(path = CLIENTES)
    List<ClienteDTO> buscar(
            BuscarClienteDTO buscarClienteDTO
    );

}

