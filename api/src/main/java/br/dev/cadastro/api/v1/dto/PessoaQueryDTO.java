package br.dev.cadastro.api.v1.dto;

import lombok.*;

@Getter
@Setter
@ToString
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class PessoaQueryDTO {

    private Integer pagina;

    private Integer totalPagina;

}
