package br.dev.cadastro.api.v1.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ClienteParametrosAlteracaoDTO {

    private String nome;
    private String cpf;
    private LocalDate dataNascimento;

}
