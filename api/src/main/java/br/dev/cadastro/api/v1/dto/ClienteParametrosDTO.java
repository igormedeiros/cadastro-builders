package br.dev.cadastro.api.v1.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;

@Getter
@Setter
public class ClienteParametrosDTO {

    @NotBlank
    private String nome;

    @NotBlank
    private String cpf;

    @PastOrPresent
    private LocalDate dataNascimento;

}
