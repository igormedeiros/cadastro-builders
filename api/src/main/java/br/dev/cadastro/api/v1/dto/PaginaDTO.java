package br.dev.cadastro.api.v1.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Collections;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class PaginaDTO<T> {

    private Integer numero;

    private Integer totalPaginas;

    private Long totalElementos;

    private Integer total;

    private List<T> lista = Collections.emptyList();

}
