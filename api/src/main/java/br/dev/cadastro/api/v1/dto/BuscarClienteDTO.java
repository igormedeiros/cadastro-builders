package br.dev.cadastro.api.v1.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuscarClienteDTO {

    private String nome;
    private String cpf;

}
