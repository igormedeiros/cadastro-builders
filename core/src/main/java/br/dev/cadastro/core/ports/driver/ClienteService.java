package br.dev.cadastro.core.ports.driver;

import br.dev.cadastro.core.domain.Cliente;
import br.dev.cadastro.core.domain.Pagina;
import br.dev.cadastro.core.domain.PessoaQuery;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.util.List;

@Validated
public interface ClienteService {

    Cliente criar(
            @NotBlank String nome,

            @NotNull @NotBlank String numeroDocumento,
            @PastOrPresent @NotNull LocalDate dataNascimento
    );

    Pagina<Cliente> listar(PessoaQuery query);

    void deletar(
            @NotNull
                    Integer idCliente
    );

    Cliente desativar(
            @NotNull
                    Integer idCliente
    );

    Cliente ativar(
            @NotNull
                    Integer idCliente
    );

    Cliente alterar(
            @NotNull
                    Integer idCliente,
            String cpf,
            String nome,
            LocalDate dataNascimento
    );

    List<Cliente> buscar(
            String nome,
            String cpf
    );
}
