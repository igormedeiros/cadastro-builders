package br.dev.cadastro.core.ports.driven;

import br.dev.cadastro.core.domain.Cliente;
import br.dev.cadastro.core.domain.Pagina;
import br.dev.cadastro.core.domain.PessoaQuery;

import java.util.List;
import java.util.Optional;

public interface ClienteRepository {

    List<Cliente> buscar(String nome, String cpfn);

    Cliente salvar(Cliente cliente);

    Pagina<Cliente> listar(PessoaQuery query);

    void deletar(Cliente cliente);

    Optional<Cliente> recuperar(Integer idPessoa);
}
