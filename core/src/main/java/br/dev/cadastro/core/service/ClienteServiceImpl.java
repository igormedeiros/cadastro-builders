package br.dev.cadastro.core.service;

import br.dev.cadastro.core.domain.Cliente;
import br.dev.cadastro.core.domain.Pagina;
import br.dev.cadastro.core.domain.PessoaQuery;
import br.dev.cadastro.core.ports.driven.ClienteRepository;
import br.dev.cadastro.core.ports.driver.ClienteService;
import br.dev.cadastro.core.util.Assert;
import br.dev.cadastro.core.util.Mensagem;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
@Service
public class ClienteServiceImpl implements ClienteService {

    private final ClienteRepository clienteRepository;

    @Override
    @Transactional
    public Cliente criar(String nome, String numeroDocumento, LocalDate dataNascimento) {
        Cliente novaCliente = Cliente
                .builder()
                .dataNascimento(dataNascimento)
                .nuremoDocumento(numeroDocumento)
                .nome(nome)
                .build();

        return clienteRepository.salvar(novaCliente);
    }

    @Override
    public Pagina<Cliente> listar(PessoaQuery query) {
        return clienteRepository.listar(query);
    }

    @Override
    @Transactional
    public void deletar(Integer idCliente) {
        Cliente cliente = clienteRepository.recuperar(idCliente).orElse(null);

        Assert.isNotNull(cliente, Mensagem.ERRO_AO_RECUPERAR_CLIENTE);

        clienteRepository.deletar(cliente);
    }

    @Override
    @Transactional
    public Cliente desativar(Integer idCliente) {
        Cliente cliente = clienteRepository.recuperar(idCliente).orElse(null);

        Assert.isNotNull(cliente, Mensagem.ERRO_AO_RECUPERAR_CLIENTE);
        cliente.desativar();
        return clienteRepository.salvar(cliente);
    }

    @Transactional
    @Override
    public Cliente ativar(Integer idCliente) {
        Cliente cliente = clienteRepository.recuperar(idCliente).orElse(null);

        Assert.isNotNull(cliente, Mensagem.ERRO_AO_RECUPERAR_CLIENTE);
        cliente.ativar();
        return clienteRepository.salvar(cliente);
    }

    @Transactional
    @Override
    public Cliente alterar(Integer idCliente, String cpf, String nome, LocalDate dataNascimento) {
        Cliente cliente = clienteRepository.recuperar(idCliente).orElse(null);

        Assert.isNotNull(cliente, Mensagem.ERRO_AO_RECUPERAR_CLIENTE);

        alterarCliente(cliente, cpf, nome, dataNascimento);

        return clienteRepository.salvar(cliente);
    }

    @Override
    public List<Cliente> buscar(String nome, String cpf) {
        return clienteRepository.buscar(nome, cpf);
    }

    private void alterarCliente(Cliente cliente, String cpf, String nome, LocalDate dataNascimento) {

        if (!Objects.isNull(cpf) && !cpf.isEmpty()) {
            cliente.setCpf(cpf);
        }
        if (!Objects.isNull(nome) && !nome.isEmpty()) {
            cliente.setNome(nome);
        }
        if (!Objects.isNull(dataNascimento)) {
            cliente.setDataNascimento(dataNascimento);
        }

    }
}
