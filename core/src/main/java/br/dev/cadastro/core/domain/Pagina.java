package br.dev.cadastro.core.domain;

import lombok.*;

import java.util.Collections;
import java.util.List;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Pagina<T> {

    private Integer numero;

    private Integer totalPaginas;

    private Long totalElementos;

    private Integer total;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<T> lista = Collections.emptyList();

}
