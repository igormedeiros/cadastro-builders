package br.dev.cadastro.core.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PessoaQuery {

    private Integer pagina;

    private Integer totalPagina;


}
