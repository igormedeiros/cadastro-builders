package br.dev.cadastro.core.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "Tb_Cliente")
public class Cliente {

    public static final int NAME_MAX_LENGTH = 200;
    public static final int CPF_LENGTH = 11;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    @NotBlank
    @Size(max = NAME_MAX_LENGTH)
    @Column(name = "nome", nullable = false, length = NAME_MAX_LENGTH)
    private String nome;
    @CPF
    @NotBlank
    @Column(name = "cpf", nullable = false, length = CPF_LENGTH)
    private String cpf;
    @NotNull
    @Column(name = "datanascimento", nullable = false)
    private LocalDate dataNascimento;
    @Column(name = "Ativo", nullable = false)
    private Boolean ativo;

    @Builder
    public Cliente(String nome, String nuremoDocumento, LocalDate dataNascimento) {
        this.nome = nome;
        this.cpf = nuremoDocumento;
        this.dataNascimento = dataNascimento;
        this.ativo = Boolean.TRUE;
    }

    public void desativar() {
        this.ativo = Boolean.FALSE;
    }

    public void ativar() {
        this.ativo = Boolean.TRUE;
    }

}
