package br.dev.cadastro.core.util;

public class Assert {

    public static void isNotNull(Object value, MessageCode messageCode, Object... messageParams) {
        if (value == null)
            throw new BusinessException(messageCode, messageParams);
    }

    public static void isNotNull(Object value, MessageCode messageCode) {
        if (value == null)
            throw new BusinessException(messageCode);
    }

}
