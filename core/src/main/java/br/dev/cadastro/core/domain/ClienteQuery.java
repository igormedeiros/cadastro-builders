package br.dev.cadastro.core.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteQuery {

    private String nome;
    private String cpf;

}
