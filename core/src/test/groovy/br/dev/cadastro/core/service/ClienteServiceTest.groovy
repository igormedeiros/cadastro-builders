package br.dev.cadastro.core.service

import br.dev.cadastro.core.domain.Cliente
import br.dev.cadastro.core.ports.driven.ClienteRepository
import br.dev.cadastro.core.util.BusinessException
import br.dev.cadastro.core.util.Mensagem
import br.dev.cadastro.test.TestSupport
import spock.lang.Unroll

import java.time.Month

import static br.dev.cadastro.test.TestUtils.*
import static java.lang.Boolean.TRUE
import static java.time.LocalDate.of

class ClienteServiceTest extends TestSupport {

    ClienteRepository clienteRepository = Mock()

    ClienteServiceImpl service = new ClienteServiceImpl(
            clienteRepository
    )

    @Unroll
    def "Deve criar cliente com sucesso"() {
        given:
        clienteRepository.salvar(_ as Cliente) >> mockCliente(id, ativo, cpf, dataNascimento, nome)

        when:
        def clienteCriado = service.criar(nome, cpf, dataNascimento)

        then:
        clienteCriado != null
        clienteCriado.ativo == ativo
        clienteCriado.cpf == cpf
        clienteCriado.dataNascimento == dataNascimento
        clienteCriado.nome == nome

        where:
        id | ativo | cpf           | dataNascimento           | nome
        1  | TRUE  | "91330735030" | of(1990, Month.APRIL, 1) | "Joaquim Barbosa"
        2  | TRUE  | "55327872041" | of(1980, Month.APRIL, 1) | "Joao de Tal"
        3  | TRUE  | "81789912091" | of(1999, Month.APRIL, 1) | "Pedro Alvares"
        4  | TRUE  | "00485148021" | of(1991, Month.APRIL, 1) | "Bob Uncle"

    }

    @Unroll
    def "Deve listas clientes com sucesso"() {
        given:
        clienteRepository.listar(_) >> mockPaginaPessoa(id, ativo, cpf, dataNascimento, nome)

        when:
        def clientes = service.listar(mockPessoaQuery())

        then:
        clientes.getLista().get(0).ativo == ativo
        clientes.getLista().get(0).cpf == cpf
        clientes.getLista().get(0).dataNascimento == dataNascimento
        clientes.getLista().get(0).nome == nome

        where:
        id | ativo | cpf           | dataNascimento           | nome
        1  | TRUE  | "91330735030" | of(1990, Month.APRIL, 1) | "Joaquim Barbosa"

    }

    def "deve deletar cliente com sucesso"() {
        given:
        clienteRepository.recuperar(_) >> Optional.of(mockCliente(id, ativo, cpf, dataNascimento, nome))
        service.deletar(id)

        where:
        id | ativo | cpf           | dataNascimento           | nome
        1  | TRUE  | "91330735030" | of(1990, Month.APRIL, 1) | "Joaquim Barbosa"

    }

    def "deve falhar ao tentar deletar cliente que nao existe"() {
        given:
        clienteRepository.recuperar(_) >> Optional.ofNullable()

        when:
        service.deletar(12)

        then:
        def e = thrown(BusinessException)
        e.messageCode == Mensagem.ERRO_AO_RECUPERAR_CLIENTE
    }

    def "Deve desativar cliente com sucesso"() {
        given:
        clienteRepository.recuperar(_) >> Optional.of(mockCliente(id, ativo, cpf, dataNascimento, nome))
        clienteRepository.salvar(_ as Cliente) >> mockCliente(id, ativo, cpf, dataNascimento, nome)

        when:
        def clienteDesativado = service.desativar(id)

        then:
        clienteDesativado != null

        where:
        id | ativo | cpf           | dataNascimento           | nome
        1  | TRUE  | "91330735030" | of(1990, Month.APRIL, 1) | "Joaquim Barbosa"
    }

    def "Deve ativar cliente com sucesso"() {
        given:
        clienteRepository.recuperar(_) >> Optional.of(mockCliente(id, ativo, cpf, dataNascimento, nome))
        clienteRepository.salvar(_ as Cliente) >> mockCliente(id, ativo, cpf, dataNascimento, nome)

        when:
        def clienteDesativado = service.ativar(id)

        then:
        clienteDesativado != null

        where:
        id | ativo | cpf           | dataNascimento           | nome
        1  | TRUE  | "91330735030" | of(1990, Month.APRIL, 1) | "Joaquim Barbosa"
    }

}
