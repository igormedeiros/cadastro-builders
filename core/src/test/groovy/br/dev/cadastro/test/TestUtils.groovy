package br.dev.cadastro.test

import br.dev.cadastro.core.domain.Cliente
import br.dev.cadastro.core.domain.Pagina
import br.dev.cadastro.core.domain.PessoaQuery

import java.time.LocalDate

class TestUtils {

    static Cliente mockCliente(Integer id, Boolean ativo, String cpf, LocalDate dataNascimento, String nome) {
        def cliente = new Cliente()
        cliente.setId(id)
        cliente.setAtivo(ativo)
        cliente.setCpf(cpf)
        cliente.setDataNascimento(dataNascimento)
        cliente.setNome(nome)
        cliente
    }

    static Pagina<Cliente> mockPaginaPessoa(Integer id, Boolean ativo, String cpf, LocalDate dataNascimento, String nome) {
        List<Cliente> clientes = [mockCliente(id, ativo, cpf, dataNascimento, nome)]

        return new Pagina<>(
                1,
                1,
                1.longValue(),
                1,
                clientes
        )
    }

    static PessoaQuery mockPessoaQuery() {
        def query = new PessoaQuery()
        query.setPagina(1)
        query.setTotalPagina(1)
    }

}
