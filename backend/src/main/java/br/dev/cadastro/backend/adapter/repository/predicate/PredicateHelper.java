package br.dev.cadastro.backend.adapter.repository.predicate;

import com.querydsl.core.types.dsl.BooleanExpression;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Function;

public abstract class PredicateHelper extends PredicateBuilder {

    protected final <T> void safeBuildQuery(final Function<T, BooleanExpression> expression, final T expressionArgs) {

        Optional.ofNullable(expressionArgs).ifPresent(arg -> {
            if (!(expressionArgs instanceof Collection<?>) || !((Collection<?>) expressionArgs).isEmpty()) {
                and(expression.apply(expressionArgs));
            }
        });
    }

}
