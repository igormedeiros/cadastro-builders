package br.dev.cadastro.backend.adapter.repository.jpa;

import br.dev.cadastro.backend.adapter.repository.predicate.ClientePredicate;
import br.dev.cadastro.core.domain.Cliente;
import br.dev.cadastro.core.domain.Pagina;
import br.dev.cadastro.core.domain.PessoaQuery;
import br.dev.cadastro.core.ports.driven.ClienteRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClienteJpaRepository extends ClienteRepository, JpaRepository<Cliente, Integer>, QuerydslPredicateExecutor<Cliente> {

    @Override
    default List<Cliente> buscar(String nome, String cpf) {
        final BooleanExpression predicate = ClientePredicate
                .create()
                .withCpf(cpf)
                .withNome(nome)
                .build();

        return Optional
                .ofNullable(predicate)
                .map(this::findAll)
                .map(c -> {
                    List<Cliente> result = new ArrayList<>();
                    c.forEach(result::add);
                    return result;
                })
                .orElse(findAll());
    }

    @Override
    default Cliente salvar(Cliente cliente) {
        return save(cliente);
    }

    @Override
    default Optional<Cliente> recuperar(Integer idPessoa) {
        return findById(idPessoa);
    }

    @Override
    default void deletar(Cliente cliente) {
        delete(cliente);
    }

    @Override
    default Pagina<Cliente> listar(PessoaQuery query) {
        PageRequest pageRequest = PageRequest.of(
                Optional.ofNullable(query).map(PessoaQuery::getPagina).orElse(0),
                Optional.ofNullable(query).map(PessoaQuery::getTotalPagina).orElse(100)
        );
        Page<Cliente> clientePage = findAll(pageRequest);

        return new Pagina<>(
                clientePage.getNumber(),
                clientePage.getTotalPages(),
                clientePage.getTotalElements(),
                clientePage.getSize(),
                clientePage.getContent()
        );
    }
}
