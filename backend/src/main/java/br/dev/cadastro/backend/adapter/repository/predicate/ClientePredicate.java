package br.dev.cadastro.backend.adapter.repository.predicate;

import br.dev.cadastro.core.domain.QCliente;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@NoArgsConstructor(staticName = "create")
public class ClientePredicate extends PredicateHelper {

    private static final QCliente qCliente = QCliente.cliente;

    public ClientePredicate withCpf(final String cpf) {
        if (cpf != null) {
            and(qCliente.cpf.eq(cpf));
        }
        return this;
    }

    public ClientePredicate withNome(String nome) {
        if (StringUtils.isNotBlank(nome)) {
            and(qCliente.nome.containsIgnoreCase(nome));
        }
        return this;
    }

}
