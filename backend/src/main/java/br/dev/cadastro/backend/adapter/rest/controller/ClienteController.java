package br.dev.cadastro.backend.adapter.rest.controller;

import br.dev.cadastro.api.v1.ClienteRestService;
import br.dev.cadastro.api.v1.dto.*;
import br.dev.cadastro.backend.adapter.mapper.ClienteMapper;
import br.dev.cadastro.core.domain.Cliente;
import br.dev.cadastro.core.domain.Pagina;
import br.dev.cadastro.core.domain.PessoaQuery;
import br.dev.cadastro.core.ports.driver.ClienteService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ClienteController implements ClienteRestService {

    private final ClienteService clienteService;
    private final ClienteMapper clienteMapper;

    @Override
    public ClienteDTO criar(ClienteParametrosDTO parametros) {
        Cliente cliente = clienteService.criar(
                parametros.getNome(),
                parametros.getCpf(),
                parametros.getDataNascimento()
        );
        return clienteMapper.toDto(cliente);
    }

    @Override
    public void deletar(Integer idCliente) {
        clienteService.deletar(idCliente);
    }

    @Override
    public ClienteDTO alterar(Integer idCliente, ClienteParametrosAlteracaoDTO parametros) {
        Cliente cliente = clienteService.alterar(
                idCliente,
                parametros.getCpf(),
                parametros.getNome(),
                parametros.getDataNascimento()
        );
        return clienteMapper.toDto(cliente);
    }

    @Override
    public PaginaClienteDTO listar(PessoaQueryDTO queryDTO) {
        PessoaQuery query = clienteMapper.toEntity(queryDTO);
        Pagina<Cliente> paginaPessoa = clienteService.listar(query);
        return clienteMapper.toDto(paginaPessoa);
    }


    @Override
    public ClienteDTO desativar(Integer idCliente) {
        return clienteMapper.toDto(clienteService.desativar(idCliente));
    }

    @Override
    public ClienteDTO ativar(Integer idCliente) {
        return clienteMapper.toDto(clienteService.ativar(idCliente));
    }

    @Override
    public List<ClienteDTO> buscar(BuscarClienteDTO buscarClienteDTO) {
        return clienteMapper.toDtos(
                clienteService.buscar(
                        buscarClienteDTO.getNome(),
                        buscarClienteDTO.getCpf()
                )
        );
    }

}
