package br.dev.cadastro.backend.adapter.repository.predicate;

import com.querydsl.core.types.dsl.BooleanExpression;

public class PredicateBuilder {

    protected BooleanExpression predicate;

    protected void and(BooleanExpression expression) {
        if (predicate != null) {
            predicate = predicate.and(expression);
        } else {
            predicate = expression;
        }
    }

    public BooleanExpression build() {
        return predicate;
    }

}
