package br.dev.cadastro.backend.adapter.mapper;

import br.dev.cadastro.api.v1.dto.ClienteDTO;
import br.dev.cadastro.api.v1.dto.PaginaClienteDTO;
import br.dev.cadastro.api.v1.dto.PessoaQueryDTO;
import br.dev.cadastro.core.domain.Cliente;
import br.dev.cadastro.core.domain.Pagina;
import br.dev.cadastro.core.domain.PessoaQuery;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Mapper(componentModel = "spring",
        imports = {
                LocalDate.class,
                ChronoUnit.class
        })
public interface ClienteMapper {

    @Mapping(target = "idade", expression = "java(Math.toIntExact(ChronoUnit.YEARS.between(cliente.getDataNascimento(), LocalDate.now())))")
    ClienteDTO toDto(Cliente cliente);

    List<ClienteDTO> toDtos(List<Cliente> clientes);

    PessoaQuery toEntity(PessoaQueryDTO queryDTO);

    PaginaClienteDTO toDto(Pagina<Cliente> pessoa);

}
