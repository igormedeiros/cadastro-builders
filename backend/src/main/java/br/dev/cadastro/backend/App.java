package br.dev.cadastro.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("br.dev.cadastro.core.domain")
@ComponentScan("br.dev.cadastro")
@EnableJpaRepositories({"br.dev.cadastro.backend.adapter.repository.jpa"})
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

}
