package br.dev.cadastro.backend.config;

import br.dev.cadastro.core.util.BusinessException;
import br.dev.cadastro.core.util.MessageCode;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestControllerAdvice
@RequiredArgsConstructor
public class ExceptionHandler extends ResponseEntityExceptionHandler {

    private final MessageSource messageSource;

    @org.springframework.web.bind.annotation.ExceptionHandler({BusinessException.class})
    public ResponseEntity<Error> handleBusinessException(BusinessException ex, WebRequest request) {
        return getError(BAD_REQUEST, ex.getMessageCode(), ex.getParams());
    }

    public ResponseEntity<Error> getError(HttpStatus status, MessageCode messageCode, Object... params) {
        return ResponseEntity.status(status).body(
                Error.error(
                        status,
                        messageCode,
                        messageSource.getMessage(messageCode.code(), params, messageCode.code(), LocaleContextHolder.getLocale())
                )
        );
    }

}
