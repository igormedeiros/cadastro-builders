package br.dev.cadastro.backend.config;

import br.dev.cadastro.core.util.MessageCode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(staticName = "error")
public class Error {

    Integer status;
    String code;
    String message;

    public static Error error(HttpStatus status, MessageCode code, String message) {
        return error(status.value(), code.code(), message);
    }

}
