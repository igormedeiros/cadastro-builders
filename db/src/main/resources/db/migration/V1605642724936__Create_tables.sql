    create table ${schema}.Tb_Cliente
(

    Id              int          NOT NULL GENERATED ALWAYS AS IDENTITY,
    Nome            varchar(200) NOT NULL,
    Cpf             varchar(11)  NOT NULL,
    DataNascimento  date         NOT NULL,
    Ativo           boolean          NOT NULL

);